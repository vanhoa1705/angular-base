FROM node:14.15-alpine AS build

ARG env

WORKDIR /app
COPY package.json .
COPY package-lock.json .
RUN npm install -g @angular/cli
RUN npm install
COPY . .

RUN if [[ "$env" = "prod" ]] ; then ng build --configuration production --aot ; else ng build ; fi

FROM nginx:stable-alpine
COPY --from=build /app/nginx.default.conf /etc/nginx/conf.d/default.conf
COPY --from=build /app/dist/web /usr/share/nginx/html/web
