import { HeaderComponent } from './components/header/header.component';
import { TableComponent } from './components/table/table.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { InputComponent } from './components/input/input.component';
import { ButtonComponent } from './components/button/button.component';
import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FeatherModule } from 'angular-feather';
import {
  ChevronLeft,
  ChevronRight,
  ChevronDown,
  Home,
  Users,
  Clipboard,
  Menu,
  LogOut,
} from 'angular-feather/icons';
import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { NavigationComponent } from './components/navigation/navigation.component';
import { MatButtonModule } from '@angular/material/button';
import { TemplateComponent } from './template/template.component';
import { MatMenuModule } from '@angular/material/menu';
import { AvatarModule } from 'ngx-avatar';
import { HttpClientModule } from '@angular/common/http';

const icons = {
  Home,
  ChevronLeft,
  ChevronRight,
  ChevronDown,
  Users,
  Clipboard,
  Menu,
  LogOut,
};

@NgModule({
  declarations: [
    AppComponent,
    ButtonComponent,
    InputComponent,
    PaginationComponent,
    TableComponent,
    NavigationComponent,
    TemplateComponent,
    HeaderComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FeatherModule.pick(icons),
    FormsModule,
    ReactiveFormsModule,
    SnotifyModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    NgxPaginationModule,
    MatButtonModule,
    MatMenuModule,
    AvatarModule,
    HttpClientModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    { provide: 'SnotifyToastConfig', useValue: ToastDefaults },
    SnotifyService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
