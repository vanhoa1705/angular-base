import { Component, Input, OnInit } from '@angular/core';
import { TypeButton } from './button.model';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent implements OnInit {
  typeButton = TypeButton;
  @Input() typeId?: TypeButton = this.typeButton.CreateButton;
  @Input() disabled?: boolean = false;

  constructor() {}

  ngOnInit(): void {}
}
