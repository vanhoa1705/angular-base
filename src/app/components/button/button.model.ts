export enum TypeButton {
  CreateButton,
  UpdateButton,
  DeleteButton,
  CancelButton,
  InviteButton,
}
