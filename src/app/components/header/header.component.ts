import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  profile = {
    firstName: 'Hoa',
    lastName: 'Nguyen',
    avatar: './assets/images/logo_godeneyes.png',
  };

  constructor() {}

  ngOnInit(): void {}

  logout(): void {}
}
