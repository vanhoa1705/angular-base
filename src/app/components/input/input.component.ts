import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator,
} from '@angular/forms';
import {
  Component,
  EventEmitter,
  forwardRef,
  Input,
  OnInit,
  Output,
} from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => InputComponent),
    },
  ],
})
export class InputComponent implements ControlValueAccessor {
  @Input() required = false;
  @Input() type: string = 'text';
  @Input() matSuffix!: string;
  @Output() matSuffixClick: EventEmitter<any> = new EventEmitter<any>();
  @Input() matPrefix!: string;
  @Output() matPrefixClick: EventEmitter<any> = new EventEmitter<any>();

  public formControl = new FormControl('');

  private onChange!: (name: string) => void;
  private onTouched!: () => void;

  constructor() {}

  writeValue(obj: any): void {
    this.formControl.setValue(obj);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {}

  doInput() {
    this.onChange(this.formControl.value);
  }

  doBlur() {
    this.onTouched();
  }

  matSuffixClicked() {
    this.matSuffixClick.emit();
  }

  matPrefixClicked() {
    this.matSuffixClick.emit();
  }
}
