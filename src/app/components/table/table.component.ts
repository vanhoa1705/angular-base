import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TableHeader } from './table.model';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  @Input() titles: TableHeader[] = [];
  @Input() data: any;
  @Input() pagination: any;
  @Output() rowClicked: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  update(row: any) {
    this.rowClicked.emit(row);
  }
}
