import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TableHeader } from '../components/table/table.model';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.scss'],
})
export class TemplateComponent implements OnInit {
  itemsPerPage = 10;
  currentPage = 1;

  titles: TableHeader[] = [
    {
      name: 'Account',
      field: 'username',
    },
    {
      name: 'Full Name',
      field: 'fullName',
    },
    {
      name: 'Last Time Updated',
      field: 'createdDateTime',
    },
  ];

  data = [
    {
      username: 'a',
      fullName: 'Nguyen Van A',
      createdDateTime: '29/10/2021',
    },
    {
      username: 'b',
      fullName: 'Nguyen Van B',
      createdDateTime: '29/11/2021',
    },
    {
      username: 'c',
      fullName: 'Nguyen Van C',
      createdDateTime: '29/12/2021',
    },
  ];
  createAccount!: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.createAccount = this.formBuilder.group({
      'first-name': ['', Validators.required],
      'last-name': ['', Validators.required],
      username: ['', Validators.required],
    });
  }

  ngOnInit(): void {}

  log(temp: any) {
    console.log(temp);
  }
}
