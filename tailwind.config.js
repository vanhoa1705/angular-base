module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        blue: {
          450: '#5F99F7',
          110: "#2F80ED",
          120: "#2F80ED",
          130:"#0E4DA4"
        },
        gray: {
          150: '#BDBDBD',
          160: '#E5E5E5',
          170: "#F2F2F2",
          180: "#828282",
          190: "#4F4F4F",
          210:"#263238",
          
        
        },
        yellow: {
          110: '#F2C94C',
          120: '#ffc000',
        }
      },
      fontSize: {
        sm: ['20px'],
        xs: ['18px'],
        s1: ['14px'],
        s10: ['10px'],
        s12: ['12px'],
        s24: ['24px'],
        s22: ['22px'],
        s16: ['16px'],
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
